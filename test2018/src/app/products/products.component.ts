import { Component, OnInit } from '@angular/core';
import { ProductsService } from "../products.service";

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;
  productsKeys=[];
  constructor(private service:ProductsService) {
    service.getProducts().subscribe(response=>
    {
      //console.log(response);
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
    //let service = new UsersService();
    })
  }

  ngOnInit() {
  }

}
