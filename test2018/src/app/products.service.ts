import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from './../environments/environment';
import {AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ProductsService {

  getProducts()
  {
    //let token='?token='+localStorage.getItem('token');
    return this.http.get('http://localhost/angular/slim/products'/*+token*/);
  }





  constructor(private http:Http, private db:AngularFireDatabase) { }

}
