// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase: {
    apiKey: "AIzaSyC_h_XhyC9c_0myQy_O9R9EiOzG22faokg",
    authDomain: "test2018firebase.firebaseapp.com",
    databaseURL: "https://test2018firebase.firebaseio.com",
    projectId: "test2018firebase",
    storageBucket: "test2018firebase.appspot.com",
    messagingSenderId: "591966743479"
  }
};



